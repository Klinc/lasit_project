import 'package:flutter/material.dart';

final ThemeData lasitTheme = ThemeData(
  primarySwatch: LasitColors.blue,
  primaryColor: LasitColors.blue[500],
  primaryColorBrightness: Brightness.dark,
  accentColor: LasitColors.black,

  backgroundColor: LasitColors.blue[50],



  inputDecorationTheme: InputDecorationTheme(
    filled: false,
    border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(12.0))),
  ),
);

class LasitColors{
  LasitColors._();

  static const errorRed = const Color(0xFFC5032B);
  static const backgroundLight = const Color(0xFFFFFFFF);
  static const backgroundDark = const Color(0xFFF5F5F5);

  static const MaterialColor black = const MaterialColor(
      0xFF262626,
      const <int, Color>{
        50: const Color(0xFFF5F5F5),
        100: const Color(0xFFE9E9E9),
        200: const Color(0xFFD9D9D9),
        300: const Color(0xFFC4C4C4),
        400: const Color(0xFF9D9D9D),
        500: const Color(0xFF7B7B7B),
        600: const Color(0xFF555555),
        700: const Color(0xFF434343),
        800: const Color(0xFF262626), //PRIMARY
        900: const Color(0xFF000000)
      }
  );

  static const MaterialColor blue = const MaterialColor(
      0xff183cdc,
      const <int, Color>{
        50: const Color(0xffe8e9fd),
        100: const Color(0xffc5c8f9),
        200: const Color(0xff9da4f4),
        300: const Color(0xff7180f1),
        400: const Color(0xff4d63ed),
        500: const Color(0xff1d45e7),
        600: const Color(0xff183cdc),
        700: const Color(0xff0030cf),
        800: const Color(0xff0024c4),
        900: const Color(0xff0001b4)
      }
  );
}