import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:lesit_test/helper_files/auth.dart';
import 'package:lesit_test/theme.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

enum FormMode {LOGIN, SIGNUP}

class LoginScreen extends StatefulWidget{
  LoginScreen({ @required this.auth, @required this.onSignedIn});
  final BaseAuth auth;
  final VoidCallback onSignedIn;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}


class _LoginScreenState extends State<LoginScreen> {

  String _email;
  String _password;

  //String _errorMessage;
  bool _isLoading;
  FormMode _formMode = FormMode.LOGIN;
  final _formKey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();

  final FocusNode _passwordInputNode = FocusNode();
  final FocusNode _passwordRepeatInputNode = FocusNode();

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _passwordController.dispose();
    super.dispose();
  }

  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget _showLogo() {
    return new Hero(
      tag: 'logo',
      child: CircleAvatar(
        backgroundColor: Theme.of(context).primaryColor,
        radius: 100.0,
        child: Container(child: Image.asset('assets/LasitLogo.png', scale: 5,)),
      ),
    );
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(height: 0.0, width: 0.0,);
  }

  Widget _showUsernameInput() {
    return Container(
      child: new TextFormField(
        decoration: InputDecoration(
          labelText: 'Email',
          prefixIcon: new Icon(Icons.mail),
        ),
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        validator: (value) =>
        value.isEmpty
            ? 'Please type in your Email'
            : null,
        onSaved: (value) => _email = value,
        textInputAction: TextInputAction.next,
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(_passwordInputNode);
        },
      ),
    );
  }

  Widget _showPasswordInput(context) {
    return Container(
      child: new TextFormField(
        decoration: InputDecoration(
          labelText: 'Password',
          prefixIcon: new Icon(Icons.lock),
        ),
        maxLines: 1,
        obscureText: true,
        validator: (value) {
          if(value.isEmpty){
            return 'Please type in your Password';
          }else return null;
        },
        onSaved: (value) => _password = value,
        textInputAction: _formMode == FormMode.LOGIN ? TextInputAction.done : TextInputAction.next,
        focusNode: _passwordInputNode,
        onFieldSubmitted: (value) {
          if (_formMode == FormMode.LOGIN){
            _validateAndSubmit(context);
          } else{
            FocusScope.of(context).requestFocus(_passwordRepeatInputNode);
          }
        },
        controller: _passwordController,
      ),
    );
  }

  Widget _showRepeatPasswordInput(context) {
    if (_formMode == FormMode.SIGNUP) {
      return Container(
        child: new TextFormField(
          decoration: InputDecoration(
            labelText: 'Repeat password',
            prefixIcon: new Icon(Icons.lock),
          ),
          maxLines: 1,
          obscureText: true,
          validator: (value) {
            print(value+"########################"+_passwordController.text);
            if (value.isEmpty){
              return 'Please repeat your Password';
            }else if (value != _passwordController.text){
              return 'Passwords don\'t match';
            }else{
              return null;
            }
          },
          onSaved: (value) => _password = value,
          textInputAction: TextInputAction.done,
          focusNode: _passwordRepeatInputNode,
          onFieldSubmitted: (value) {
            _validateAndSubmit(context);
          },
        ),
      );
    }else return new Container();
  }

  Widget _showPrimButton(BuildContext context) {
    return Container(
      child: RaisedButton(
        onPressed: () {
          _validateAndSubmit(context);
        },
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12.0))),
        color: Theme.of(context).accentColor,
        padding: EdgeInsets.symmetric(vertical: 15.0),
        child: Text(_formMode == FormMode.LOGIN ? 'LOGIN' : 'CREATE ACCOUNT', 
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16.0),),
      ),
    );
  }

  Widget _showSecButton(BuildContext context) {
    return FlatButton(
      onPressed: _formMode == FormMode.LOGIN
          ? _changeFormToSignUp
          : _changeFormToLogin,
      padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 50.0),
      child: _formMode == FormMode.LOGIN ?
      Text(
          'Don\'t have an account? Sign up.',
          style: new TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300)
      ) : new Text(
          'Have an account? Sign in.',
          style: new TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300)
      ),
    );
  }

  Widget _loginForm(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _showUsernameInput(),
          Padding(padding: EdgeInsets.symmetric(vertical: 5.0)),
          _showPasswordInput(context),
          Padding(padding: EdgeInsets.symmetric(vertical: 5.0)),
          _showRepeatPasswordInput(context),
          Padding(padding: EdgeInsets.symmetric(vertical: 20.0)),
          _showPrimButton(context),
          Padding(padding: EdgeInsets.symmetric(vertical: 5.0)),
          _showSecButton(context),
          Padding(padding: EdgeInsets.symmetric(vertical: 10.0)),
        ],
      ),
    );
  }

  void _changeFormToSignUp() {
    _formKey.currentState.reset();
    _passwordController.clear();
    setState(() {
      _formMode = FormMode.SIGNUP;
    });
  }

  void _changeFormToLogin() {
    _formKey.currentState.reset();
    _passwordController.clear();
    setState(() {
      _formMode = FormMode.LOGIN;
    });
  }

  void _showErrSnackbar(BuildContext context, String message) {
    if (message.length > 0 && message != null) {
      final errSnackbar = SnackBar(
        content: Text(message),
        backgroundColor: Theme
            .of(context)
            .colorScheme
            .error,
        duration: Duration(seconds: 2),
      );
      Scaffold.of(context).showSnackBar(errSnackbar);
    }
  }

  _validateAndSubmit(BuildContext context) async {
    setState(() {
      _isLoading = true;
    });
    if (_validateAndSave()) {
      String userId;
      try {
        if (_formMode == FormMode.LOGIN) {
          FirebaseUser tempUser = await widget.auth.signIn(_email, _password);
          userId = tempUser.uid;
          print('Signed in: $userId');
        } else if (_formMode == FormMode.SIGNUP) {
          FirebaseUser tempUser = await widget.auth.signUp(_email, _password);
          userId = tempUser.uid;
          print('Signed up user: $userId');
        }
        if (userId.length > 0 && userId != null) {
          widget.onSignedIn();
        }
      } catch (e) {
        print('Error: $e');
        if (Theme
            .of(context)
            .platform == TargetPlatform.iOS) {
          _showErrSnackbar(context, e.details);
        } else
          _showErrSnackbar(context, e.message);
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Builder(
                builder: (context) =>
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 26.0, vertical: 8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Padding(padding: EdgeInsets.symmetric(vertical: 40.0)),
                          _showLogo(),
                          Padding(padding: EdgeInsets.symmetric(vertical: 80.0)),
                          _loginForm(context),
                        ],
                      ),
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}