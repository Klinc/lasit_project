import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ToolsScreen extends StatefulWidget {
  @override
  _ToolsScreenState createState() => _ToolsScreenState();
}

class _ToolsScreenState extends State<ToolsScreen> {
  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
        stream: Firestore.instance.collection('test').snapshots(),
        builder: (context, snapshot){
          if(!snapshot.hasData) return Center(child: const CircularProgressIndicator());
          return ListView.builder(
            itemCount: snapshot.data.documents.length,
              itemBuilder: (context, index){
                return _buildListitem(context, snapshot.data.documents[index]);
              }
          );
        }
      ),
    );
  }

  Widget _buildListitem(BuildContext context, DocumentSnapshot document) {
    return ListTile(
      title: Text(document['name']),
      subtitle: Text(document['value']),
    );
  }
}
