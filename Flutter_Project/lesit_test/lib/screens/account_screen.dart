import 'package:flutter/material.dart';

class AccountScreen extends StatefulWidget{
  AccountScreen(VoidCallback onSignedOut);

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  Widget _userInfo(){
    return Container(
      child: Column(
        children: <Widget>[
          new Container(
            color: Theme.of(context).accentColor,
            height: 300,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Account'),
      ),
      body: Column(
        children: <Widget>[
          Container(
            child: _userInfo(),
          ),
        ],
      ),
    );
  }
}