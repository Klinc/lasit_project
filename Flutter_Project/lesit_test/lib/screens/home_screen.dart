import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'tools_screen.dart';
import 'locations_screen.dart';
import 'parts_screen.dart';
import 'account_screen.dart';
import 'package:lesit_test/helper_files/auth.dart';


class HomeScreen extends StatefulWidget {
  HomeScreen({this.title, this.auth, this.userId, this.onSignedOut});

  final String title;
  final BaseAuth auth;
  final String userId;
  final VoidCallback onSignedOut;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with SingleTickerProviderStateMixin {

  TabController _tabController;
  final List<Tab> tabs = <Tab>[
    Tab(text: 'Tools'),
    Tab(text: 'Parts'),
    Tab(text: 'Crates'),
  ];


  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: tabs.length, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lesit"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(context: context, delegate: DataSearch());
            },
          ),
          IconButton(
            icon: Icon(Icons.account_circle),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context)=> AccountScreen(widget.onSignedOut))
              );
            },
          ),
        ],
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: Colors.white,
          //indicator: BorderTabIndicator(insets: EdgeInsets.all(30.0)),
          tabs: tabs,
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          ToolsScreen(),
          PartsScreen(),
          LocationsScreen(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(MdiIcons.qrcodeScan),
        elevation: 10.0,
      ),
    );
  }
}

class DataSearch extends SearchDelegate<String> {
  final searchData = [
    "Jože",
    "francka",
    "lolipop",
    "crka",
    "Janez",
    "Beseda",
    "In",
    "Podobna",
    "jajčka"
  ];

  final recentSearch = ["lolipop", "crka", "Janez", "Beseda"];

  @override
  List<Widget> buildActions(BuildContext context) {
    //actions for app bar
    return query.isEmpty
        ? null
        : [
            IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                query = '';
              },
            )
          ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    //leading icon on the left of the appBar
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    // show some result based on the selection
    return Center(
      child: Container(
        height: 100.0,
        width: 100.0,
        child: Card(
            child: Center(
          child: Text("hello"),
        )),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // show when someone searches for something
    final suggestionList = query.isEmpty
        ? recentSearch
        : searchData
            .where((p) => p.toLowerCase().startsWith(query.toLowerCase()))
            .toList();

    return ListView.builder(
      itemBuilder: (context, index) => ListTile(
            onTap: () {
              showResults(context);
            },
            leading: CircleAvatar(
              child: Text(suggestionList[index][0]),
            ),
            title: Text(suggestionList[index]),
          ),
      itemCount: suggestionList.length,
    );
  }
}
