/*import 'dart:async';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';


class CodeScanFAB extends StatefulWidget{

  @override
  _CodeScanFABState createState() => _CodeScanFABState();
}

class _CodeScanFABState extends State<CodeScanFAB> {
  String barcode = "";

  @override
  initState() {
    super.initState();
  }

  Widget floatingCodeScanButton(){
    return FloatingActionButton(
      child: Icon(MdiIcons.qrcodeScan),
        onPressed: scan,
    );
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException{
      setState(() => this.barcode = 'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return floatingCodeScanButton();
  }*/
//}