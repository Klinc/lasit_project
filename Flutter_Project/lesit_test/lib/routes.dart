import 'package:flutter/material.dart';
import 'screens/login_screen.dart';
import 'package:lesit_test/helper_files/auth.dart';
import 'package:lesit_test/screens/home_screen.dart';

class RoutePage extends StatefulWidget {
  RoutePage({@required this.auth});

  final BaseAuth auth;

  @override
  State<StatefulWidget> createState() => new _RoutePageState();
}

enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}

class _RoutePageState extends State<RoutePage> {
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  String _userId;

  @override
  void initState() {
    super.initState();
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        if (user != null) {
          _userId = user.uid;
        }
        authStatus = _userId == null ? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
      });
    });
  }

  void _onLoggedIn(context) {
    print("_onLoggedIn()" + "###################################################################");
    widget.auth.getCurrentUser().then((user){
      _userId = user.uid;
      authStatus = AuthStatus.LOGGED_IN;
      Navigator.pushReplacement(context,
          MaterialPageRoute(
              builder: (context)=> HomeScreen(
                userId: _userId,
                auth: widget.auth,
                onSignedOut: (){_onSignedOut(context);},
              )
          )
      );
    });

  }

  void _onSignedOut(context) {
    widget.auth.signOut();
    authStatus = AuthStatus.NOT_LOGGED_IN;
    _userId = null;
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context)=> LoginScreen(
            auth: widget.auth,
            onSignedIn:(){_onLoggedIn(context);},
          ),
        )
    );

  }

  Widget _buildWaitingScreen() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print(authStatus.toString() + "###################################################################");
    switch (authStatus) {
      case AuthStatus.NOT_DETERMINED:
        return _buildWaitingScreen();
        break;
      case AuthStatus.NOT_LOGGED_IN:
        return new LoginScreen(
          auth: widget.auth,
          onSignedIn: (){_onLoggedIn(context);},
        );
        break;
      case AuthStatus.LOGGED_IN:
        if (_userId.length > 0 && _userId != null) {
          return new HomeScreen(
            userId: _userId,
            auth: widget.auth,
            onSignedOut: (){_onSignedOut(context);},
          );
        } else return _buildWaitingScreen();
        break;
      default:
        return _buildWaitingScreen();
    }
  }
}