import 'package:flutter/material.dart';
import 'routes.dart';
import 'package:flutter/services.dart';
import 'theme.dart';
import 'package:lesit_test/helper_files/auth.dart';

void main(){
  /*SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.light
    ),
  );*/ //transparent system overlay
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp
  ]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Lasit',
      theme: lasitTheme,
      home: RoutePage(auth: Auth()),
    );
  }
}
